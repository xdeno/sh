import literal from "https://gitlab.com/xdeno/utils/-/raw/0.0.1/literal.js";
import * as io from "https://deno.land/std@0.148.0/io/mod.ts";
import {
  isFunction,
  isPlainObject,
} from "https://cdn.skypack.dev/lodash-es@4.17.21";

async function sh(command, opts = {}) {
  const {
    stdin,
    verbosity,
    json,
    lines,
    split,
    ok,
    code,
    result,
    shell,
    ignoreErrors,
    interactive,
    env,
    generator,
  } = opts;
  if (verbosity > 1) {
    console.error(command);
  }
  if (stdin && interactive) {
    throw "stdin is not supported with 'interactive' option";
  }
  if (generator && !lines) {
    throw "generator is supported only with 'lines' option";
  }

  let tmp;
  try {
    const cmd = { env };
    if (!interactive) {
      cmd.stdin = "piped";
      if (verbosity) {
        tmp = await Deno.makeTempFile();
        const match = command.match(/^(.*)\s+(&|&!|;)\s*$/);
        if (match) {
          command = `${match[1]} | tee ${tmp} ${match[2]}`;
        } else {
          command += ` | tee ${tmp}`;
        }
      } else {
        cmd.stdout = "piped";
        cmd.stderr = "piped";
      }
    }
    if (opts.hasOwnProperty("shell")) {
      if (shell) {
        cmd.cmd = [shell, "-c", command];
      } else {
        cmd.cmd = command.split(" ");
      }
    } else {
      cmd.cmd = ["/bin/sh", "-c", command];
    }

    const proc = Deno.run(cmd);

    if (stdin) {
      await proc.stdin.write(new TextEncoder().encode(stdin));
      proc.stdin.close();
    }

    if (generator && lines) {
      return io.readLines(proc.stdout);
    }

    let stdoutText;
    let stderrText;
    if (!interactive && !verbosity) {
      const stdout = await proc.output();
      stdoutText = new TextDecoder().decode(stdout).trim();
      const stderr = await proc.stderrOutput();
      stderrText = new TextDecoder().decode(stderr).trim();
    }

    const status = await proc.status();
    proc.close();

    if (!interactive && verbosity) {
      stdoutText = await Deno.readTextFile(tmp);
    }

    if (!status.success && !ignoreErrors && !ok) {
      const errorMsg =
        `Error code: ${status.code} command: ${command} stderr: ${stderrText} stdout: ${stdoutText}`;
      throw new Error(errorMsg);
    }

    let res = stdoutText;
    if (lines && stdoutText) {
      res = res.split("\n");
    }
    if (split && stdoutText) {
      res = res.split(split);
    }
    if (json && stdoutText) {
      if (ignoreErrors) {
        try {
          res = JSON.parse(stdoutText);
        } catch {}
      } else {
        res = JSON.parse(stdoutText);
      }
    }
    if (code) {
      return status.code;
    } else if (ok) {
      return status.success;
    } else if (result) {
      return { status, stderr: stderrText, stdout: res };
    } else {
      return res;
    }
  } finally {
    if (tmp) {
      await Deno.remove(tmp);
    }
  }
}

function shellLit(strings, ...keys) {
  return literal(strings, ...keys)
    .replaceAll(/(^\s*\n\s*|\s*\n\s*$)/g, "")
    .replaceAll(/\n\s*/g, " ");
}

function shWrapper(stringsOrOpts, ...keys) {
  if (Array.isArray(stringsOrOpts)) {
    const command = shellLit(stringsOrOpts, ...keys);
    return sh(command, this);
  } else {
    return setShFlags(shWrapper.bind({ ...this, ...stringsOrOpts }), this);
  }
}

function setShFlags(s, defaults = {}) {
  const flags = {
    v: { verbosity: 1 },
    vv: { verbosity: 2 },
    i: { ignoreErrors: true },
    it: { interactive: true },
    result: true,
    json: true,
    lines: true,
    generator: true,
    ok: true,
    code: true,
    split: undefined,
    stdin: undefined,
    env: undefined,
    shell: undefined,
  };
  return setFlags(s, flags, shWrapper, defaults);
}

// TODO: refactor
export function setFlags(s, flags, orig, defaults = {}) {
  for (const [flag, opts] of Object.entries(flags)) {
    if (opts === undefined) {
      s[flag] = (arg) => {
        const opts2 = { ...defaults, [flag]: arg };
        return setFlags(orig.bind(opts2), flags, orig, opts2);
      };
    } else {
      Object.defineProperty(s, flag, {
        get: function () {
          let opts2;
          if (isPlainObject(opts)) {
            const opts1 = {};
            for (const [k, v] of Object.entries(opts)) {
              if (isFunction(v)) {
                opts1[k] = v(defaults[k]);
              } else {
                opts1[k] = v;
              }
            }
            opts2 = { ...defaults, ...opts1 };
          } else {
            opts2 = { ...defaults, [flag]: opts };
          }
          return setFlags(orig.bind(opts2), flags, orig, opts2);
        },
      });
    }
  }
  return s;
}

export default setShFlags(shWrapper.bind({}));
